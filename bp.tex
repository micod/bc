\documentclass[12pt,oneside]{fithesis2}
\usepackage[czech]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[inline]{enumitem}
\usepackage[                      % A sans serif font that blends well with Palatino
  scaled=0.86
]{berasans}
\usepackage[                      % A tt font if you do not like LM's tt
  scaled=1.03
]{inconsolata}
\usepackage[                      % Clickable links
  plainpages = false,               % We have multiple page numberings
  pdfpagelabels                     % Generate pdf page labels
]{hyperref}

\usepackage{subfig}
\usepackage{enumerate}
\usepackage{bbding} % checkmarks
\usepackage{booktabs} % professional looking tables
\usepackage{subfig}
\usepackage{titlesec}
\usepackage{url}

\usepackage[
   backend=biber      % if we want unicode 
%  ,style=iso-numeric %iso-authoryear % or iso-numeric for numeric citation method          
%  ,babel=other        % to support multiple languages in bibliography
%  ,sortlocale=cs_CZ   % locale of main language, it is for sorting
%  ,bibencoding=UTF8   % this is necessary only if bibliography file is in different encoding than main document
]{biblatex}

% url breaks in bibliography
\setcounter{biburllcpenalty}{7000}
\setcounter{biburlucpenalty}{8000}

\setlength{\bibhang}{0pt}

\thesislang{cs}                   % The language of the thesis
\thesistitle{Sledování hokejistů ve videozáznamu}       % The title of the thesis
\thesissubtitle{Bakalářská práce}  % The type of the thesis
\thesisstudent{Michal Plch}          % Your name
\thesiswoman{false}                % Your gender
\thesisfaculty{fi}                % Your faculty
\thesisyear{jaro \the\year}     % The academic term of your thesis defense
\thesisadvisor{doc. RNDr. Pavel Matula, Ph.D.}   % Your advisor

\linespread{1.3}

\clubpenalty 10000          % prvni radek odstavce nebude sam na konci stranky
\widowpenalty 10000         % posl. radek odstavce nepujde na novou stranku
\hyphenpenalty 5000
\tolerance 1000

\def\figurename{Obr.}

\addbibresource{bibliography.bib}

\begin{document}
\FrontMatter                    % The front matter
\ThesisTitlePage                % The title page

\begin{ThesisDeclaration}       % The declaration
      \DeclarationText
      \AdvisorName
\end{ThesisDeclaration}

\begin{ThesisThanks}            % The acknowledgements (optional)
Rád bych poděkoval vedoucímu práce doc. RNDr. Pavlu Matulovi, Ph.D. za cenné odborné rady a~za čas, který mi byl ochoten věnovat. Také bych chtěl poděkovat doc. RNDr. Petru Holubovi, Ph.D. za spolupráci při vzniku této práce a~společnosti \textit{Daite s.r.o.} za poskytnutí záznamu hokejového utkání o~požadované kvalitě.
\end{ThesisThanks}

\begin{ThesisAbstract}          % The abstract
Cílem této práce je navrhnout a~implementovat algoritmus pro sledování pohybu hokejistů v~záznamu hokejového utkání pořízeném statickou kamerou. Součástí práce je popis všech fází zpracování jednotlivých snímků videa, zhodnocení přesnosti detekce, klasifikace a~sledování hráčů, grafická aplikace určená k~testování vlastností navrhovaného algoritmu a~popis použitých technologií. Získané poznatky z~oblasti sledování objektů ve videu mají sloužit jako základ pro tvorbu komplexního systému pro sledování hráčů při sportovních utkáních s~využitím více kamer.
\end{ThesisAbstract}

\begin{ThesisKeyWords}
zpracování obrazu, matematická morfologie, detekce pohybu, optimální přiřazení, tracking, OpenCV, Qt
\end{ThesisKeyWords}

\tableofcontents                % The table of contents  

\MainMatter                     % The main matter

\chapter{Úvod}
Digitální zpracování obrazu je informatický obor, díky kterému je možné získávat informace z~optického záznamu reálného světa. Se zvyšováním výkonu a~dostupnosti informační techniky nachází tento obor uplatnění v~široké škále odvětví, např. v~medicíně, průmyslové výrobě nebo ve sportu.

Zřejmě nejznámějším příkladem využití digitálního zpracování obrazu ve sportovním prostředí je systém Hawk-Eye\footnote{Angl. \textit{jestřábí oko}.} \cite{hawkeye}, který pomocí soustavy kamer dokáže přesně určit pozici hracího míče na herní ploše. Dalším příkladem úspěšného nasazení počítačů při analýze sportovního zápasu je systém SportVU \cite{sportvu}, využívající 6~kamer ke sledování pohybu hráčů a~míče při basketbalovém utkání. Získaná data se využívají k~rozhodování sporných situací,  výpisu podrobných statistik celého zápasu nebo srovnání výkonu jednotlivých hráčů.

Cílem této práce je navrhnout algoritmus, který dokáže na záznamu hokejového utkání pořízeném statickou kamerou rozpoznat postavy pohybující se po ledové ploše, přiřadit je ke správnému týmu (domácí, hosté nebo rozhodčí), a~sledovat jejich pohyb v~obraze. Ke zpřesnění sledování jednotlivých hráčů bude využit předpoklad o~plynulém setrvačném pohybu hráčů v~hokeji. Součástí práce je také aplikace, která navrhovaný algoritmus implementuje a~poskytuje uživatelské rozhraní pro jeho testování. Při návrhu algoritmu a~jeho implementaci je kladen důraz na výkonnost výsledného řešení.

Kapitola 2 obsahuje obecný popis navrhovaného algoritmu a~jednotlivých fází, ze kterých se algoritmus skládá. Kapitola 3 popisuje parametry, kterými je možné měnit vlastnosti sledovacího algoritmu, a~na ukázkovém videozáznamu demonstruje vliv těchto parametrů na výkon aplikace a~přesnost sledování. V kapitole 4 je představen software nazvaný Observer\footnote{Angl. \textit{pozorovatel}.}, který navrhovaný algoritmus implementuje a~poskytuje rozhraní k~nastavení jeho parametrů včetně možnosti vizualizace průběhu algoritmu.

Poznatky získané na základě této práce poslouží jako základ pro návrh uceleného řešení pro sledování sportovních zápasů, využívajícího ke sledování hráčů obraz z~více kamer a~schopného generovat podrobné statistiky o~utkání.

Všechny binární obrázky, demonstrující průběh jednotlivých fází algoritmu, jsou v~práci z~důvodů přehlednosti a~úspory při tisku invertovány.

\chapter{Popis algoritmu}
Zpracování obrazu videozáznamu je rozděleno na několik podúloh. Těmito úlohami jsou \textit{segmentace pohybujících se objektů}, \textit{úprava nalezených objektů}, \textit{klasifikace komponent} a~\textit{sledování hráčů}. K~vyřešení  úlohy sledování hokejistů v~obraze je tedy potřeba vyřešit tyto čtyři nezávislé podúlohy.

\section{Segmentace pohybujících se objektů}
Předpokladem pro detekci pohybu ve videozáznamu je staticky umístěná kamera. Pokud se kamera nehýbe, potom pixely, jejichž hodnota se v~čase výrazně nemění, náleží nepohyblivému \textit{pozadí} scény. Naopak pixely, u~kterých se jejich hodnota mění často a~výrazně, náleží pohyblivému \textit{popředí}.

Algoritmus pro detekci pohybujících se objektů ve scéně musí disponovat řadou vlastností. Předně musí zvládnout detekovat hokejisty i~poté, co se na krátkou chvíli zastaví. Bez této vlastnosti by program stojící hráče okamžitě po zastavení přiřadil ke statickému pozadí scény, tj. přestal by je vidět. Dále musí detekovat stíny vrhané hokejisty na ledovou plochu. Stín na ledové ploše vyvolá významnou změnu v~barvách pixelů pozadí v~okolí hráče a~jejich přiřazení k~popředí. Hledaný algoritmus by měl být schopný takovýmto nežádoucím přiřazením zabránit.
 Poslední požadovanou vlastností algoritmu je průběžné přizpůsobení měnícím se světelným podmínkám. Typickými změnami v~obraze záznamu hokejového utkání na hokejovém stadionu souvisejícími s~osvětlením jsou např. změna intenzity osvětlení v~hale nebo změna zrcadlových odlesků světel od plochy ledu po jeho povrchové úpravě.

Všechny tyto požadavky splňuje algoritmus fungující na principu \textit{směsi gaussiánů}.
\begin{quotation}\textit{,,Základním postupem je přiřazení dat v~každém snímku do několika gaussiánů. Hledáme tedy K~normálních (Gaussových) rozdělení pravděpodobnosti, která nejlépe reprezentují naše data. Každý pixel je ale modelován samostatně a~má tak vlastní parametry jednotlivých gaussiánů. Jednotlivé gaussiány se v~čase aktualizují. Lze tedy předpokládat, že si takovýto model poradí s~šumem i~plynulými změnami osvětlení.``} \cite{gauss}
\end{quotation}

Výstupem algoritmu pro detekci pohybu v~obraze je šedotónový obraz, viz obrázek \ref{fig:motion}. Bíle\footnote{Pro přehlednost je v~textu popisován význam černé a~bílé barvy na zobrazených obrázcích po inverzi barev.} jsou v~tomto obraze obarveny pixely, kde k~žádnému pohybu nedochází. Černé pixely představují místa s~výraznými změnami v~barvách pixelů, což algoritmus interpretuje jako pohyb. Pro určení pixelů stínu detektor pohybu porovná novou hodnotu pixelu s~hodnotou ve statistickém modelu pozadí, a~pokud je barevný odstín stejný, ale nová hodnota pixelu je tmavší než pixel v~modelu pozadí, pak předpokládá, že došlo k~zastínění pozadí a~pixel označí šedou barvou jako stín.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{pictures/motion}
    \caption{Výstup algoritmu pro detekci pohybu.}
    \label{fig:motion}
\end{figure}

\section{Úprava nalezených objektů}
\textit{Maska popředí}, která je výstupem segmentace pohybu, obsahuje vady, které je potřeba odstranit. Za vady jsou v~této fázi zpracování obrazu považovány přítomnost stínu rozpoznaného detektorem pohybu, přítomnost šumu a~nevyhovující tvar komponent popředí.

\subsection{Odstranění stínů}
Při sledování objektů ve scéně je potřeba rozlišit pixely pozadí od pixelů popředí, stíny tedy nenesou žádnou užitečnou informaci a~je potřeba je z~obrazu odfiltrovat. Toho lze docílit binárním prahováním obrazu, které šedé pixely stínu nahradí bílou barvou a~tím je přiřadí k~pozadí, viz obrázek \ref{fig:threshold}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{pictures/threshold}
    \caption{Maska popředí po odprahování stínů.}
    \label{fig:threshold}
\end{figure}

\subsection{Odfiltrování šumu}
Šum je náhodná změna hodnot pixelů v~obraze. Na množství šumu má vliv např. úroveň osvětlení scény nebo kvalita snímacího zařízení (tj. kamery). Protože je zašuměný obraz vstupem detektoru pohybu, je šum interpretován jako pohyb a~v~masce popředí je zobrazen jako černé pixely.

Ke zpracování binárního obrazu je výhodné použít operátory definované \textit{matematickou morfologií}\footnote{Matematická morfologie je teorie zabývající se analýzou prostorových struktur.}. Odstranění šumu zajistí použití \textit{morfologického otevření} se \textit{strukturním elementem} malého průměru. Dojde tím k~odstranění drobných shluků pixelů typických pro šum, ale nedojde k~poškození větších komponent, viz obrázek \ref{fig:opening}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{pictures/opening}
    \caption{Maska popředí po odstranění šumu.}
    \label{fig:opening}
\end{figure}

\subsection{Rekonstrukce tvaru komponent}
Komponenty popředí v~této fázi zpracování obrazu vykazují několik vad: obsahují díry, a~často je v~obraze místo jedné komponenty představující jednoho hráče několik menších komponent. Za tyto vady může způsob, jakým detektor pohybu rozpoznává stín. Díry v~komponentách vzniknou tak, že jsou jako stín interpretovány bílé části dresů hráčů pohybujících se po podobně bílém ledu.

Protože v~předchozím kroku byly stíny z~obrazu odstraněny, je nutné tyto díry v~komponentách zacelit. K~tomu je možné použít další operátor matematické morfologie: \textit{morfologické uzavření}. Jeho použitím dojde k~zacelení děr v~komponentách, viz obrázek \ref{fig:closing}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{pictures/closing}
    \caption{Maska popředí po zacelení děr v~komponentách.}
    \label{fig:closing}
\end{figure}

\section{Klasifikace komponent}
Vstupem této fáze je binární obraz obsahující komponenty pohybujících se objektů ve scéně. Tento obraz je následně zpracován algoritmem \textit{značkování komponent}, který projde celý obraz a~každé komponentě přiřadí unikátní identifikační číslo. Navíc je pro každou komponentu vypočítán i~její střed, plocha a~ohraničující obdélník. Úkolem této fáze je určit, které komponenty představují hráče, a~následně je přiřadit ke správnému týmu.

\subsection{Rozpoznání komponent hráčů}
V~masce popředí je třeba rozlišit komponenty hráčů od nevýznamných vad v~obraze. Algoritmus využívá skutečnosti, že velikost hráčů ve scéně se pohybuje v~určitém rozmezí. Proto jsou z~dalšího zpracování vyřazeny komponenty příliš malé, kterými jsou typicky pozůstatky šumu, a~příliš velké, které se ve scéně objevují při náhlých změnách osvětlení. Objekty splňující kritérium velikosti jsou považovány za postavy pohybující se po ledové ploše.

\subsection{Určení týmu}
U~postav v~obraze je důležité rozlišit, jestli se jedná o~hráče jednoho ze dvou hrajících týmů nebo o~hokejové rozhodčí. Této informace algoritmus využívá ve fázi sledování hráčů. 

Rozdělení do týmů se děje na základě barev dresů hráčů a~rozhodčích. Segmentaci barvy z~obrazu je vhodné provést v~barevném prostoru HSV\footnote{Hue, Saturation, Value -- barevný model sestávající z~odstínu, sytosti a~jasu barvy.}. Hledaná barva je programu zadána jako dva trojrozměrné vektory, představující minimální a~maximální hodnoty kanálů HSV. Pokud všechny tři složky zkoumaného pixelu v~barevném prostoru HSV spadají do rozsahů vymezených vstupními vektory, potom pixel obsahuje hledanou barvu a~je přidán do binární \textit{masky barvy}. Pokud má dres více výrazných barev, jsou tyto barvy z~obrazu segmentovány zvlášť a~poté operátorem logického součtu složeny do jedné masky. Tento přístup má tu výhodu, že nehledá jednu konkrétní barvu, ale skupinu podobných barev, takže si poradí i~s~méně kvalitním obrazem a~proměnnými světelnými podmínkami.

Pro každou komponentu hráče jsou z~obrazu segmentovány všechny tři skupiny barev: barvy domácího týmu, hostů a~rozhodčích. Masky barev jsou poté virtuálně přiloženy přes pixely masky popředí zkoumané komponenty. Pokud některé z~těchto přiložení splňuje předem stanovenou úroveň procentuálního překrytí, znamená to, že v~komponentě je dominantní barva jednoho z~týmů a~objekt hráče je k~tomuto týmu přiřazen, jinak je hráč ponechán bez týmu.

Objekty obsahující informace o~nalezených komponentách hráčů jsou uloženy do pole pro další zpracování.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=1]{pictures/color}
    \hspace{10pt}
    \includegraphics[scale=1]{pictures/green}
    \caption{Ukázka segmentace zelené barvy z~dresu hráče.}
    \label{fig:color}
\end{figure}

\section{Sledování hráčů}
Sledování pohybu hráčů ve videozáznamu se dá popsat jako úloha \textit{optimálního přiřazení}. Cílem optimálního přiřazení je nalézt taková přiřazení v~úplném bipartitním grafu, kde součet jejich vah je co nejmenší. V~tomto případě jsou dvěmi disjunktními množinami, ze kterých se bipartitní graf skládá, množina nově nalezených hráčů na snímku v~čase $t$ a~množina již nalezených hráčů na snímcích v~časech $t-k$, kde $k$ nabývá hodnot od $1$ do doby, po kterou jsou v~paměti uchováváni hráči bez vhodného přiřazení. Váhami mezi uzly bipartitního grafu jsou vzdálenosti hráčů v~časech $t-k$ od hráčů v~čase $t$. Cílem optimálního přiřazení v~této úloze je tedy přiřadit hráčům v~časech $t-k$ jejich novou pozici, na které se v~obraze nalézají v~čase~$t$.

Do grafu jsou také přidány tzv. \textit{dummy}\footnote{Angl. \textit{figurína}, \textit{atrapa}.} uzly. Pro $n$ sledovaných hráčů v~časech $t-k$ a~$m$ nově nalezených hráčů v~čase $t$ je do grafu přidáno $m$ dummy uzlů k~hráčům v~časech $t-k$ a~$n$ dummy uzlů k~hráčům v~čase $t$. Dále je třeba před samotným přiřazováním penalizovat taková přiřazení, která v~dané úloze nedávají smysl. Penalizací je myšleno nastavení váhy na tak vysokou hodnotu, že bude dané přiřazení znevýhodněno před všemi smysluplnými přiřazeními. Pokud pro daného hráče neexistuje žádné smysluplné přiřazení, je napojen na jeden z~dummy uzlů. Ke kontrole smysluplnosti přiřazení slouží informace o~vzdálenosti, příslušnosti hráče k~týmu a~jeho vektoru rychlosti.

Vzdálenosti hráčů mezi snímky se využívá k~zamítnutí takových přiřazení, které přesahují předem stanovenou maximální vzdálenost. 

Když se algoritmu podaří určit, ke kterému týmu hráč patří, je tato informace k~hráči přiřazena trvale po celou dobu jeho sledování. Poté jsou akceptována pouze taková přiřazení, která by nezařadila hráče k~jinému týmu.

Vektor rychlosti je počítán jako průměr vektorů mezi několika posledními známými souřadnicemi hráče. Na základě vektoru rychlosti jsou zamítnuta taková přiřazení, podle kterých by hráč náhle změnil směr pohybu (úhel mezi původním a~aktualizovaným vektorem by byl větší než předem stanovená tolerance) nebo rychlost (aktualizovaný vektor by měl výrazně rozdílnou délku než vektor původní).

K~výpočtu optimálního přiřazení slouží \textit{Maďarský algoritmus} \cite{munkres}, který funguje s~kubickou časovou složitostí a~úlohu řeší optimálním způsobem. Jeho vstupem je matice vzdáleností mezi hráči v~časech $t$ a~$t-k$, výstupem je vektor představující optimální přiřazení mezi těmito množinami hráčů.

Na závěr program projde seznamy již sledovaných a~nově nalezených hráčů. Pokud nalezne hráče, který již byl sledován, ale kterému nebylo v~průběhu několika posledních snímků záznamu nalezeno odpovídající přiřazení, znamená to, že hráč ze scény odjel,  a~tak jej odstraní ze seznamu a~přestane sledovat. Všichni nově nalezení hráči, pro které nebyl nalezen protějšek v~minulosti, jsou přidáni do seznamu sledovaných hráčů. Poté začne algoritmus zpracovávat další snímek videa.


\chapter{Nastavení parametrů algoritmu}
Vlastnosti algoritmu popsaného v~první kapitole závisí na řadě parametrů, které ovlivňují přesnost segmentace a~sledování hráčů a~jeho výkon. Při volbě hodnot parametrů je také potřeba brát v~potaz umístění kamery a~vlastnosti snímané scény, jako např. světelné podmínky nebo nežádoucí zdroje pohybu ve scéně.

Výkon je měřen na počítači s~moderním procesorem řady Intel Core i7. Za dostatečný výkon se považuje schopnost algoritmu na daném hardware zpracovávat video v~reálném čase, tedy s~minimální frekvencí 25 snímků za sekundu.

Cílem této kapitoly je nalézt nejvhodnější hodnoty pro tyto parametry na ukázkové scéně a~význam jednotlivých parametrů ukázat na obrázcích.

\section{Zkoumaná scéna}
Videozáznam, který slouží k~testování navrženého sledovacího algoritmu, pochází z~hokejového stadionu v~Třinci. Jak je na snímku \ref{fig:scene} vidět, hokejové utkání proti sobě hrají týmy Třince (červené dresy) a~Karlových Varů (zeleno-bílé dresy). Dresy rozhodčích jsou tradičně černo-bílé. Kamera je umístěná pod zobrazovací kostkou a~namířená směrem k~jedné z~branek, zabírá tedy jednu polovinu hřiště.

Protože je v~záběru i~část hlediště, je nutné s~využitím binární masky hlediště zakrýt, aby detektor pohybu bral v~potaz pouze pohyb odehrávající se na hrací ploše, viz obrázek \ref{fig:mask}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{pictures/scene2}
    \caption{Snímek z~ukázkového videozáznamu.}
    \label{fig:scene}
\end{figure}

\begin{figure}[h!]
    \centering
    \subfloat[Maska hrací plochy.]
    {\includegraphics[width=0.475\textwidth]{pictures/mask}}
    \hspace{4pt}
    \subfloat[Vstupní obraz po zakrytí binární maskou.]
    {\includegraphics[width=0.475\textwidth]{pictures/masked}}
    \caption{Využití binární masky k~vyhrazení oblasti zájmu.}
    \label{fig:mask}
\end{figure}

\section{Parametry segmentace pohybujících se objektů}
V~této fázi algoritmu je důležitým údajem velikost historie snímků, kterou využívá detektor pohybu k~sestavení statistického modelu pozadí scény. Nižší hodnota vede k~přiřazení postav ve scéně k~nepohyblivému pozadí krátce po jejich zastavení, vysoká hodnota naopak ztěžuje začlenění k~pozadí objektům, které se zastaví na delší dobu. Při sledování hokejistů je vhodné zvolit větší velikost historie snímků, protože jakékoliv začlenění postavy k~pozadí je nežádoucí, ovšem změny v~osvětlení scény musí být k~pozadí přiřazeny co nejdříve.
Dobu která je potřeba k~začlenění změny v~obraze k~pozadí při různé délce historie pohybu popisuje tabulka \ref{table:history}. Detekce stojících hokejistů čekajících na vhazování je znázorněna na obrázku \ref{fig:history}. Při použití 500 referenčních snímků by se nepohybliví hráči vytráceli ze scény již po 2 sekundách, a~při použití 1000 snímků by změny v~osvětlení zůstaly v~obraze celých 7 sekund, jako vhodný kompromis se tedy jeví použití 750 referenčních snímků s~dobou začlenění nových objektů k~pozadí 5 sekund.

\begin{table}[htbp]
\begin{center}\scalebox{1}{%\tabcolsep=0.11cm
\begin{tabular}{cc}
\midrule
počet snímků historie pohybu & času nutný k~začlenení změny k~pozadí (s)\tabularnewline
\midrule
500 & 1,85\tabularnewline
750 & 5,16\tabularnewline
1000 & 7,38\tabularnewline
\bottomrule
\end{tabular}
}
\end{center}
\caption{Vliv počtu snímků historie pohybu na rychlost začlenění změny v~obraze k~pozadí.}
\label{table:history}
\end{table}

\begin{figure}[h!]
    \centering
    \subfloat[500 snímků]
    {\includegraphics[scale=1]{pictures/h500-cut}}\\
    \subfloat[750 snímků]
    {\includegraphics[scale=1]{pictures/h750-cut}}\\
    \subfloat[1000 snímků]
    {\includegraphics[scale=1]{pictures/h1000-cut}}
    \caption{Detekce hokejistů čekajících na vhazování s~využitím uvedeného počtu snímků historie pohybu.}
    \label{fig:history}
\end{figure}

\section{Parametry úpravy nalezených objektů}
Při filtrování šumu je důležité zvolit takový strukturní element (dále SE), který zajistí odstranění šumu, ale nenaruší tvar větších objektů. 
Požadovaného výsledku dosahuje SE s~malým průměrem 3~pixely, který odstraní šum a~zachová nejvíce detailů v~komponentách hráčů. SE se sudou hodnotou průměru nejsou vhodné, protože jejich použitím dochází k~posunutí obrazu. Jak je patrné z~obrázků \ref{fig:open-rec-cut} a~\ref{fig:open-circ-cut}, ilustrujících nastavení parametrů filtrace šumu, tvar SE nemá na kvalitu filtrace šumu významný vliv. Je tomu tak proto, že při takto nízkých rozměrech odpovídá čtvercový SE aproximaci kruhu. Vzhledem k~malému průměru SE tato operace není výpočetně náročná a~nebrání zpracování snímků videa v~reálném čase.

\begin{figure}[h!]
    \centering
    \subfloat[2 pixely]
    {\includegraphics[scale=0.7]{pictures/rec2-cut}}
    \hspace{2pt}
    \subfloat[3 pixely]
    {\includegraphics[scale=0.7]{pictures/rec3-cut}}
    \hspace{2pt}
    \subfloat[4 pixely]
    {\includegraphics[scale=0.7]{pictures/rec4-cut}}
    \caption{Filtrování šumu čtvercovým SE o~uvedené hraně.}
    \label{fig:open-rec-cut}
\end{figure}

\begin{figure}[h!]
    \centering
    \subfloat[2 pixely]
    {\includegraphics[scale=0.7]{pictures/circ2-cut}}
    \hspace{2pt}
    \subfloat[3 pixely]
    {\includegraphics[scale=0.7]{pictures/circ3-cut}}
    \hspace{2pt}
    \subfloat[4 pixely]
    {\includegraphics[scale=0.7]{pictures/circ4-cut}}
    \caption{Filtrování šumu kruhovým SE o~uvedeném průměru.}
    \label{fig:open-circ-cut}
\end{figure}

Rekonstrukce tvaru komponent vyžaduje dostatečně velký SE k~zacelení děr v~komponentách, ovšem při příliš velkém SE bude častěji docházet k~propojení blízkých komponent. Velmi důležitý je i~tvar SE. Při použití SE čtvercového tvaru mají komponenty ve výsledku velmi zubatý tvar, který přesně neodpovídá hráčům v~obraze. Přesnějšího tvaru výsledných komponent lze docílit použitím kruhového SE. Při této úloze ovšem přesnost segmentace nehraje významnou roli. Sledovacímu algoritmu stačí, když zná polohu hráčů v~obraze, a~z~jejího okolí dokáže vyčíst dostatek informací o~barvě dresu hráče. Navíc je náročné implementovat zpracování obrazu kruhovým SE výkonným způsobem. Při použití kruhového SE klesl počet zpracovaných snímků pod požadovanou hodnotu 25 snímků za sekundu. Pro rekonstrukci tvaru je nutné použít SE o~průměru alespoň 19 pixelů, při nižších rozměrech nedochází k~úplnému zacelení děr v~komponentách. Optimálních vlastností tedy dosahuje SE čtvercového tvaru o~hraně 19 pixelů. Nastavení parametrů rekonstrukce tvaru komponent ilustrují obrázky \ref{fig:close-rec-cut} a~\ref{fig:close-circ-cut}.

\begin{figure}[h!]
    \centering
    \subfloat[11 pixelů]
    {\includegraphics[scale=0.7]{pictures/rec11-cut}}
    \hspace{2pt}
    \subfloat[15 pixelů]
    {\includegraphics[scale=0.7]{pictures/rec15-cut}}
    \hspace{2pt}
    \subfloat[19 pixelů]
    {\includegraphics[scale=0.7]{pictures/rec19-cut}}
    \caption{Rekonstrukce tvaru komponent čtvercovým SE o~uvedené hraně.}
    \label{fig:close-rec-cut}
\end{figure}

\begin{figure}[h!]
    \centering
    \subfloat[11 pixelů]
    {\includegraphics[scale=0.7]{pictures/circ11-cut}}
    \hspace{2pt}
    \subfloat[15 pixelů]
    {\includegraphics[scale=0.7]{pictures/circ15-cut}}
    \hspace{2pt}
    \subfloat[19 pixelů]
    {\includegraphics[scale=0.7]{pictures/circ19-cut}}
    \caption{Rekonstrukce tvaru komponent kruhovým SE o~uvedeném průměru.}
    \label{fig:close-circ-cut}
\end{figure}

\section{Parametry klasifikace komponent}
Při zařazování hráčů do týmů se zkoumá procentuální překrytí masky segmentované barvy přes komponentu hráče. Procentuální úroveň nastavená příliš nízko povede k~častějšímu přiřazení hráčů do špatného týmu, při vysoké úrovni pak hráči nebudou přiřazeni do žádného týmu. Protože si sledovací algoritmus po zařazení hráče do týmu tuto informaci pamatuje trvale, je výhodné nastavit procentuální úroveň na vyšší hodnotu. Dojde tím ke snížení počtu záběrů, ze kterých bude možné tým hráče rozpoznat, ale výsledné přiřazení k~týmu bude přesnější.

Pro danou scénu jsou zvoleny hodnoty úrovně překrytí $12\%$ pro hráče v~červených dresech, $8\%$ pro hráče v~zelených dresech a~$50\%$ pro rozhodčí. Takto nízké hodnoty je nutné zvolit kvůli malému množství pixelů nesoucích požadovanou barevnou informaci v~situacích, kdy se hráč pohybuje dále od kamery.

\section{Parametry sledování hráčů}
Fáze sledování využívá parametry, které řídí přesnost samotného přiřazování hráčů mezi snímky videozáznamu. Přesnost sledování je ale obtížně kvantifikovatelná. Pro výpočet přesnosti je třeba vytvořit referenční sadu dat představující přesný pohyb osob na ledě, to by ovšem vyžadovalo manuální zpracování tisíců snímků videa. Netriviální by bylo také vyvinutí algoritmu, který by prováděl srovnání dat referenčního modelu s~daty ze sledovacího algoritmu. Proto tato práce neposkytuje přesná měření, ale přesnost sledování je hodnocena na základě pozorování chování algoritmu na testovací scéně. Dále je nutné připomenout, že sledovací algoritmus neprovádí korekci perspektivy, proto pozice hráčů na ledě představuje pozici v~souřadné soustavě obrazu, nikoliv reálnou pozici na ledové ploše. Pro účely zkoumání vlastností sledovacího algoritmu je však toto zjednodušení dostatečné.

Parametr \textit{maximální vzdálenosti} určuje, jaká největší vzdálenost komponent hráče mezi jednotlivými snímky je považována za platnou.  S~tím souvisí i~parametr \textit{trvanlivosti hráčů}, udávající dobu, po které je hráč bez platného přiřazení vymazán z~paměti. Nastavení těchto dvou parametrů teoreticky dovoluje algoritmu řešit situace, kdy dojde k~blízkému průjezdu několika hráčů. Sice by došlo ke sloučení komponent hráčů, ale po jejich opětovném rozdělení by algoritmus v~okolí našel původní komponenty a~dokázal by je zpětně přiřadit. Pokud ale hráči v~zákrytu urazí vzdálenost větší než je maximální dovolená vzdálenost přiřazení, ke zpětnému přiřazení nedojde. Zvýšení maximální přiřazovací vzdálenosti není řešením, protože by pak docházelo k~nesmyslným přiřazením na vzdálenost, kterou hráč reálně nemohl mezi snímky urazit. Nežádoucí je také udržovat v~paměti příliš dlouho hráče bez vhodného přiřazení, protože by k~nim mohli být přiřazeni jiní hráči, kteří daným místem projedou později. Vzhledem k~těmto vlastnostem a~ke složitosti a~četnosti kolizí hráčů na ledě se na tyto parametry při řešení kolizí obecně nedá spolehnout. Pro potřeby ukázkové scény jsou tyto parametry nastaveny na nízké hodnoty, maximální vzdálenost na 150 pixelů a~doba trvanlivosti hráčů na 20 snímků, aby nesnižovaly přesnost přiřazování.

Dalším parametrem sledování hráčů je jejich \textit{rychlost}. Rychlost jako vektorová veličina udává rychlost pohybu v~pixelech za snímek a~směr pohybu. Při přiřazování komponent hráčů mezi snímky se tedy porovnává změna v~rychlosti pohybu, ke které by po přijmutí daného přiřazení došlo, a~úhel mezi dosavadním a~nově vypočteným směrem pohybu, který nesmí překročit maximální dovolenou odchylku. Hokejisté jsou ovšem v~určitých situacích schopni dosáhnout prudkých změn v~rychlosti pohybu, například při rozjezdu z~místa nebo při nárazu, a~při nízké rychlosti i~prudkých změn ve směru pohybu. Je tedy nutné parametry rychlosti nastavit na velmi tolerantní hodnoty, aby nedocházelo k~zamítnutí jinak platných přiřazení. Pro hráče pohybující se velmi pomalu je dokonce nutné kontrolu rychlosti dočasně vypnout, než hráč znovu nabere rychlost. Z~výše uvedených vlastností a~z~pozorování chování algoritmu na ukázkové scéně vyplývá, že vzdálenost komponent hráčů mezi snímky videozáznamu a~barva dresu hráče poskytují dostatek informací pro sledování pohybu hráčů na ledě. Využití informace o~rychlosti hráče vede k~častému zamítání platných přiřazení, proto sledovací algoritmus funguje lépe, když je hodnocení přiřazení na základě rychlosti zcela vypnuto.

\section{Celkový výkon}
Ukázkové video, na kterém je sledovaný algoritmus testován, má rozlišení 1280x720 pixelů, a~snímkovací frekvenci 50 snímků za sekundu. Požadovaný výkon sledovacího algoritmu při zpracování tohoto videa je alespoň 25 snímků za sekundu, neboli jeden snímek za 40 ms. V~záznamu utkání byla vybrána pasáž, ve které se pohybuje velké množství hokejistů a~algoritmus tedy provádí největší množství výpočtů za snímek, a~následně byla změřena doba trvání zpracování jednoho snímku videa a~jednotlivých fází algoritmu. Časy potřebné pro provedení jednotlivých výpočtů, včetně jejich procentuálního vlivu na dobu zpracování jednoho snímku, jsou uvedeny v~tabulce \ref{table:power}. Z~této tabulky vyplývá, že výpočetně nejnáročnější operací je značkování komponent, následované detekcí pohybu a~rekonstrukcí tvaru komponent. Pod položkou ostatní se ukrývá rutinní kopírování paměti. Dobrou vlastností navrženého algoritmu je, že při časech okolo 15 ms na zpracování jednoho snímku dokáže video zpracovávat v~reálném čase.

\begin{table}[htbp]
\begin{center}\scalebox{1}{%\tabcolsep=0.11cm
\begin{tabular}{lcc}
\midrule
 & doba výpočtu (ms) & náročnost výpočtu (\%)\tabularnewline
\midrule
detekce pohybu & 2,79 & 18,33\tabularnewline
prahování & 0,19 & 1,26\tabularnewline
filtrace šumu & 0,33 & 2,18\tabularnewline
rekonstrukce tvaru komponent & 2,25 & 14,83\tabularnewline
značkování komponent & 6,03 & 39,66\tabularnewline
klasifikace hráčů & 1,40 & 9,20\tabularnewline
párování hráčů & 0,06 & 0,40\tabularnewline
ostatní & 2,15 & 14,14\tabularnewline
\bottomrule
zpracování jednoho snímku & 15,20 & 100,00\tabularnewline
\bottomrule
\end{tabular}
}
\end{center}
\caption{Výpočetní náročnost jednotlivých částí algoritmu}
\label{table:power}
\end{table}


\chapter{Program Observer}
V~rámci této bakalářské práce vznikl software nazvaný Observer, který obsahuje implementaci navrženého sledovacího algoritmu a~poskytuje k~němu jednoduché uživatelské rozhraní, viz obrázek \ref{fig:observer}. V~horní části obrazovky se nachází tlačítka ovládající přehrávané video. V~pravé části jsou přístupné parametry sledovacího algoritmu, které je možné za běhu měnit a~sledovat tak změny v~chování algoritmu. Stejně tak je možné přepínat pohled na obrázky vzniklé v~jednotlivých fázích implementovaného algoritmu, a~pořizovat snímky v~jeho průběhu. V~centrální části se zobrazuje obrazová reprezentace zvolené části výpočtu. Program dokáže i~vizualizovat průběh sledování hráčů, jejich příslušnost k~týmu a~vektor rychlosti.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{pictures/observer}
    \caption{Uživatelské rozhraní programu Observer.}
    \label{fig:observer}
\end{figure}

\section{Použité knihovny}
Při vývoji programu byl kladen důraz na výkon výsledné implementace a~využití softwarových knihoven se svobodnou licencí a~otevřeným zdrojovým kódem. Sledovací algoritmus je napsán v~jazyce C\raise .65ex \hbox{$_{++}$}, který je jako kompilovaný a~staticky typovaný jazyk ideální pro výpočetně náročné úlohy. Operace zpracování obrazu obstarává knihovna OpenCV \cite{opencv}, která poskytuje řadu nástrojů a~algoritmů určených pro úlohy zpracování obrazu a~počítačového vidění. Algoritmus pro detekci pohybu v~obraze, který je v~OpenCV implementován, vychází z~publikací \cite{density} a~\cite{mixture}. Uživatelské rozhraní je napsáno v~jazyce QML, který je součástí knihovny Qt \cite{qt}. QML umožňuje jednoduchou tvorbu uživatelského rozhraní deklarativním zápisem a~zároveň poskytuje vysoký výkon vykreslování grafiky pomocí rozhraní OpenGL. Použitá implementace Maďarského algoritmu pochází z~GitHubu \cite{github}. Uvedené knihovny jsou napsány v~jazyce C\raise .65ex \hbox{$_{++}$} a~jsou dostupné pro celou řadu softwarových a~hardwarových platforem. Program je vyvíjen na Linuxu, ale s~minimálními změnami ve zdrojovém kódu je možné jej sestavit i~pro platformy Windows, Mac OS X, Android a~iOS.


\chapter{Závěr}
V~rámci této práce se podařilo navrhnout a~implementovat algoritmus pro sledování hokejistů ve videozáznamu pořízeném statickou kamerou. Výsledný software dokáže v reálném čase detekovat hokejisty ve scéně na základě jejich pohybu, a~tuto informaci si udržet i~při jejich krátkodobém zastavení. K~tomu využívá algoritmus pro detekci pohybu v~obraze fungující na principu směsi gaussiánů. Funkční je také rozpoznávání týmu hráče na základě barev jeho dresu, které ovšem dosahuje potřebné spolehlivosti pouze pokud je hráč blízko kamery, na větší vzdálenost už v~obraze není dostatek pixelů s~požadovanou barevnou informací. Rozpoznání týmu selhává také v~případech, kdy mají dva týmy podobné barvy dresů. Sledování pohybu hráčů je řešeno jako úloha optimálního přiřazení,  kterou optimálním způsobem řeší Maďarský algoritmus. Sledovací algoritmus dokáže bez problémů sledovat samostatně se pohybující hráče, ale selhává při řešení kolizí. Heuristika snažící se po opětovném rozdělení komponent hráčů nalézt v~širším okolí nepřiřazenou komponentu funguje pouze v~některých případech, a~nedá se na ni tedy obecně spolehnout. Využití rychlosti hráčů a~předpokladu o~jejich plynulém pohybu po ledě nejen že také nepřispívá k~řešení kolizí, ale navíc znesnadňuje sledování samostatných hráčů, proto při dostatečné snímkovací frekvenci videa stačí počítat přiřazení pouze na základě aktuální pozice v~obraze. Zajímavou vlastností navrženého algoritmu je také to, že nemusí sledovat pouze hokej, ale změnou jeho parametrů je možné docílit sledování pohybu v~jiném sportu, např. fotbale, nebo sledování pohybu vozidel na křižovatkách.

Dalším krokem při vývoji sledovacího software je využití obrazu z~více kamer pokrývajících celou plochu hřiště. Snímáním scény z~různých úhlů by bylo možné lépe řešit kolize hráčů v~obraze. Stejně tak by bylo možné téměř neustále snímat čísla na dresech jednotlivých hráčů a~zlepšit tak přesnost sledování. Přidáním více kamer také naroste počet obrazových dat určených ke zpracování, proto bude nutné za účelem zachování vysokého výkonu aplikace přesunout výpočty zpracování obrazu na grafickou kartu.

\printbibliography

\begin{appendix}

\chapter{Přílohy}
\begin{itemize}
	\item Zdrojové soubory aplikace Observer
	\item Ukázkové video
	\item Video vizualizující průběh sledovacího algoritmu
	\item Zdrojové soubory práce v~jazyce LaTeX
	\item Vlastní text práce ve formátu PDF
\end{itemize}

\end{appendix}

\end{document}
